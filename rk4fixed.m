function [tout,xout] = rk4fixed(FUN,tspan,x0,Nsteps)

% Copyright (C) 1999-2019 Marc Compere
% This file is intended for use with Octave or Matlab.
% rk4fixed.m is free software; you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by
% the Free Software Foundation version 2.
%
% rk4fixed.m is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details at www.gnu.org/copyleft/gpl.html.
%
% --------------------------------------------------------------------
%
% rk4fixed (v1.16) is a 4th order Runge-Kutta numerical integration routine.
% It requires 4 function evaluations per step.
%
% 4th-order accurate RK methods have a local error estimate of O(h^5).
%
%
% Usage:
%         [tout, xout] = rk4fixed(FUN, tspan, x0, Nsteps)
%
% INPUT:
% FUN    - String containing name of user-supplied problem derivatives.
%          Call: xprime = fun(t,x) where FUN = 'fun'.
%          t      - Time or independent variable (scalar).
%          x      - Solution column-vector.
%          xprime - Returned derivative COLUMN-vector; xprime(i) = dx(i)/dt.
% tspan  - [ tstart, tfinal ]
% x0     - Initial value COLUMN-vector.
% Nsteps - number of steps used to span [ tstart, tfinal ]
%
% OUTPUT:
% tout  - Returned integration time points (row-vector).
% xout  - Returned solution, one solution column-vector per tout-value.
%
% The result can be displayed by: plot(tout, xout).
%
% Marc Compere
% CompereM@asme.org
% created : 06 October 1999
% modified: 17 March 2019


% Initialization
t = tspan(1);
h = (tspan(2)-tspan(1))/Nsteps;
xout(1,:) = x0';
tout(1) = t;
x = x0(:);
halfh = 0.5*h;

for i=1:Nsteps
     RK1 = feval(FUN,t,x);
     thalf = t+halfh;
     xtemp = x+halfh*RK1;
     RK2 = feval(FUN,thalf,xtemp);
     xtemp = x+halfh*RK2;
     RK3 = feval(FUN,thalf,xtemp);
     tfull = t+h;
     xtemp = x+h*RK3;
     RK4 = feval(FUN,tfull,xtemp);
     
     t = t + h;
     x = (x+h/6*(RK1+2.0*(RK2+RK3)+RK4));
     tout = [tout; t];
     xout = [xout; x.'];
end
