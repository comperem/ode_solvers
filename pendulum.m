% Copyright (C) 1999-2019 Marc Compere
% This file is intended for use with Octave or Matlab.
% pendulum.m is free software; you can redistribute it and/or modify it
% under the terms of the GNU General Public License as published by
% the Free Software Foundation version 2.
%
% pendulum.m is distributed in the hope that it will be useful, but
% WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
% General Public License for more details at www.gnu.org/copyleft/gpl.html.
%
% --------------------------------------------------------------------
%
% This integrates a set of ordinary differential equations (ODE) using 6 different
% ODE solvers.  The equations represent the dynamics of a simple pendulum.
%
% The integrators ode78.m, ode45.m, ode23.m, rk8fixed.m, rk4fixed.m, and rk2fixed.m
% all produce column vector output similar to Matlab results. Each takes a similar
% set of input arguments that are significantly different from the native Matlab
% ode45() couterparts.
%
% All integrators work in octave 2.1.42 and Matlab 7.0.4.365 (R14) or later
% with no modification.  They also work in some older octave or Matlab versions.
%
% Marc Compere
% comperem@asme.org
% created : 06 October 1999
% modified: 17 March 2019

clear

% allow global access to the parameters m, g, l, & b:
global m g l b

m=1;    % (kg)
g=9.81; % (m/s^2)
l=1.;   % (m)
b=0.7;   % ((N-s)/m))

% integrator setup:
t0=0;
tfinal = 5; % (seconds)
tspan = [t0,tfinal];
hmax = 0.1;

% Initial Conditions: theta(t=0)=30(deg) & initially at rest
IC = [ 30*pi/180,0]'; % (rad), (rad/s)
sps = 20;                  % sps -> step per second for the fixed solvers only
Nsteps=(tfinal-t0)*sps;    % this creates sps number of integration steps per second for fixed step solvers
tolerance = 1e-3;          % tolerance for the variable step solvers only



% Solve the ODE specified in penddot.m using each of the 6 m-file integrators
% (twice for ode45 for both 4(5) coefficient pairs).

disp('Integrating using rk2fixed...')
tic
[t1,x_rk2fixed] = rk2fixed('penddot',tspan,IC,Nsteps);     % fixed step integration
t(1)=toc;


disp('Integrating using rk4fixed...')
tic
[t2,x_rk4fixed] = rk4fixed('penddot',tspan,IC,Nsteps);     % fixed step integration
t(2)=toc;


disp('Integrating using rk8fixed...')
tic
[t3,x_rk8fixed] = rk8fixed('penddot',tspan,IC,Nsteps);     % fixed step integration
t(3)=toc;


disp('Integrating using ode23...')
tic
[t4,x_ode23] = ode23('penddot',tspan,IC,tolerance,hmax); % rk23 variable step integration
t(4)=toc;


disp('Integrating using ode45 with the Dormand-Prince 4(5) pair...')
tic
[t5,x_ode45dp] = ode45('penddot',tspan,IC,tolerance,hmax,'dormandprince'); % rk45 variable step integration
t(5)=toc;


disp('Integrating using ode45 with the Fehlberg 4(5) pair...')
tic
[t6,x_ode45rkf] = ode45('penddot',tspan,IC,tolerance,hmax,'rkf'); % rk45 variable step integration
t(6)=toc;


disp('Integrating using ode78...')
tic
[t7,x_ode78] = ode78('penddot',tspan,IC,tolerance,hmax); % rk78 variable step integration
t(7)=toc;


disp('Elapsed times for each solver to integrate the state equations for a simple pendulum:')
t


% notes: The Dormand-Prince pair in ode45 produces a time output vector
%        of 52x1.  (size(t5)=52x1)
%        The number of function evaluations is 103 which you can compute
%        from (52-1)*6+1=307.  (52-1) because there were really only 51 new
%        steps computed.  The first step is just the initial conditions.
%        Multiply (52-1) by 6 because each step during the normal main loop
%        requires 6 function evaluations to compute.  Add 1 because the very
%        first step requires 1 function evaluation to start the main loop.


% plot each result for comparison
figure(1)
clf
if ishold~=1, hold, end
title('Solution Comparison for Pendulum Position and Velocity')
ylabel('Theta and Theta-dot (rad) & (rad/s)')
xlabel('Time (s)')
h1=plot(t1,x_rk2fixed,'r');
h2=plot(t2,x_rk4fixed,'g');
h3=plot(t3,x_rk8fixed,'b');
h4=plot(t4,x_ode23,'y');
h5=plot(t5,x_ode45dp,'m');
h6=plot(t6,x_ode45rkf,'c');
h7=plot(t7,x_ode78,'k');
legend([h1(1),h2(1),h3(1),h4(1),h5(1),h6(1),h7(1)],'rk2fixed()','rk4fixed()','rk8fixed()','ode23()','ode45() DP','ode45() RKF','ode78()'); % ,'Location','SouthEast'
if ishold==1, hold, end
grid on
%print -djpeg pendulumResults

% These plots show the angular position and velocity
% trajectories created by each different integrator.
% Position is the trace that reaches steady state at -pi/2.
% Velocity reaches a steady state of zero.


% create a bar chart illustrating the relative solution speed of each solver
figure(2)
clf, %clg
if ishold~=1, hold, end
title('Solver Solution Time Comparison')
ylabel('solution time (s)')
xlabel('Solver')
h_bar=bar(t);
txtOffset = min(max(t)*0.025,0.03);
text(1-0.5 , t(1)+txtOffset ,'rk2fixed()')
text(2-0.5 , t(2)+txtOffset ,'rk4fixed()')
text(3-0.5 , t(3)+txtOffset ,'rk8fixed()')
text(4-0.5 , t(4)+txtOffset ,'ode23()')
text(5-0.5 , t(5)+txtOffset ,'ode45() DP')
text(6-0.5 , t(6)+txtOffset ,'ode45() RKF')
text(7-0.5 , t(7)+txtOffset ,'ode78()')
if ishold==1, hold, end
grid on
%print -djpeg barChart
